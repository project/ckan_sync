<?php

declare(strict_types=1);

namespace Drupal\ckan_sync;

use Drupal\Core\Config\ConfigFactoryInterface;
use Drupal\Core\Logger\LoggerChannelFactoryInterface;
use Drupal\Core\Logger\LoggerChannelInterface;
use Drupal\Core\Utility\Error;
use Psr\Http\Client\ClientExceptionInterface;
use Psr\Http\Client\ClientInterface;
use Psr\Http\Message\ResponseInterface;

/**
 * CkanClient class provide the authentication and request to CKAN resource.
 */
final class CkanClient {

  const TIMEOUT = 5;

  /**
   * Logger object.
   *
   * @var Drupal\Core\Logger\LoggerChannelInterface
   */
  private LoggerChannelInterface $logger;

  /**
   * Url to CKAN resource.
   *
   * @var string
   */
  private string $url;

  /**
   * Token to CKAN resource.
   *
   * @var string
   */
  private string $token;

  /**
   * Authentication header name.
   *
   * @var string
   */
  private string $authHeaderName;


  /**
   * Authentication http username.
   *
   * @var string
   */
  private string $httpAuthUsername;


  /**
   * Authentication http pass.
   *
   * @var string
   */
  private string $httpAuthPass;

  /**
   * Constructs a CkanClient object.
   */
  public function __construct(
    private readonly ClientInterface $transport,
    LoggerChannelFactoryInterface $logger_factory,
    ConfigFactoryInterface $config_factory
  ) {
    $this->logger = $logger_factory->get('ckan_client');

    $config = $config_factory->get('ckan_sync.settings');

    $this->url = $config->get('url') ?? '';
    $this->token = $config->get('token') ?? '';
    $this->authHeaderName = $config->get('auth_header_name') ?: 'Authorization';

    $this->httpAuthUsername = $config->get('http_auth_username') ?: '';
    $this->httpAuthPass = $config->get('http_auth_pass') ?: '';
  }

  /**
   * Perform an arbitrary action.
   */
  public function call(string $action, array $data = []): array | null {
    return $this->request('POST', $action, ['json' => $data]);
  }

  /**
   * Perform a side-effect-free action.
   */
  public function get(string $action, array $query = []): array | null {
    /** @TODO We can add logging in this place for all queries */
    return $this->request('GET', $action, ['query' => $query]);
  }

  /**
   * Call an action that requires file upload.
   */
  public function upload(string $action, array $data = []): array | null {
    return $this->request(
      'POST',
      $action,
      [
        'multipart' => array_map(
          fn($v, $k) => [],
          $data,
          array_keys($data)
        ),
      ],
      ['Content-Type' => 'multipart/form-data']
    );
  }

  /**
   * Low-level request maker.
   *
   * Prefer using more generic methods, like `get` or `call` instead.
   */
  public function request(string $method, string $action, array $options, array $headers = []): array | null {
    try {
      $response = $this->transport->request($method, "{$this->url}/api/action/{$action}", $options + [
          'timeout' => self::TIMEOUT,
          'headers' => $headers + [
              'Content-Type' => 'application/json',
              $this->authHeaderName => $this->token,
            ],
          'verify' => TRUE,
          'auth' => [
            $this->httpAuthUsername,
            $this->httpAuthPass,
          ],
        ]);
    }
    catch (ClientExceptionInterface $e) {
      \Drupal::logger('ckan_sync')->debug($e->getMessage());
      return NULL;
    }

    return $this->processResponse($response, $action);
  }

  /**
   * Convert response object into an optional result array.
   */
  private function processResponse(ResponseInterface $response, string $action): array | null {
    if ($response->getStatusCode() != 200) {
      $this->logger->error('Non-200 call to @action: @code @reason', [
        '@action' => $action,
        '@code' => $response->getStatusCode(),
        '@reason' => $response->getReasonPhrase(),
      ]);
      return NULL;
    }

    $result = json_decode($response->getBody()->getContents(), TRUE);
    if (isset($result['success'])) {
      return $result['result'];
    }

    $this->logger->debug('@action failed: @error', [
      '@action' => $action,
      '@code' => $response->getStatusCode(),
      '@error' => json_encode($result['error']
        ?? t('NO Results: 200 - But no SUCCESS status - $response->getBody()->getContents() is empty')),
    ]);

    return NULL;
  }

}
