<?php

namespace Drupal\ckan_sync\Authentication;

use Drupal\Core\Authentication\AuthenticationProviderInterface;
use Drupal\Core\Session\UserSession;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpKernel\Exception\HttpException;

/**
 * Authentication provider to validate requests in header.
 */
class RestAuth implements AuthenticationProviderInterface {

  /**
   * {@inheritdoc}
   */
  public function applies(Request $request) {
    $route_name = \Drupal::routeMatch()->getRouteName();
    if ($route_name == 'rest.menu_sructure_resource.GET') {
      return $request->headers->has('X-CKAN-API-Key');
    }
  }

  /**
   * {@inheritdoc}
   */
  public function authenticate(Request $request) {
    $ckan_settings = \Drupal::config('ckan_sync.settings');
    $ckan_token = $ckan_settings->get('token');
    $token = $request->headers->get('X-CKAN-API-Key');

    if ($token == $ckan_token) {
      // Return a session if the request passes the validation.
      return new UserSession();
    }
    else {
      return new HttpException(400, 'Specified X-CKAN-API-Key value is incorrect');
    }
  }

}
