<?php

namespace Drupal\ckan_sync\Plugin\rest\resource;

use Drupal\Core\Menu\MenuTreeParameters;
use Drupal\Core\Session\AccountProxyInterface;
use Drupal\rest\Plugin\ResourceBase;
use Drupal\rest\ResourceResponse;
use Psr\Log\LoggerInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Endpoint for menu structure resource.
 *
 * @RestResource(
 *   id = "menu_structure_resource",
 *   label = @Translation("Menu structure Resource"),
 *   uri_paths = {
 *     "canonical" = "/api/resource/menu/{name}"
 *   }
 * )
 */
class LayoutResource extends ResourceBase {

  /**
   * A current user instance.
   *
   * @var \Drupal\Core\Session\AccountProxyInterface
   */
  protected $currentUser;

  /**
   * {@inheritdoc}
   */
  public function __construct(
    array $configuration,
    $plugin_id,
    $plugin_definition,
    array $serializer_formats,
    LoggerInterface $logger,
    AccountProxyInterface $current_user) {
    parent::__construct($configuration, $plugin_id, $plugin_definition, $serializer_formats, $logger);

    $this->currentUser = $current_user;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition) {
    return new static(
      $configuration,
      $plugin_id,
      $plugin_definition,
      $container->getParameter('serializer.formats'),
      $container->get('logger.factory')->get('articles_syndication'),
      $container->get('current_user')
    );
  }

  /**
   * Responds to GET requests.
   */
  public function get($name) {
    $response = [];

    $ckan_settings = \Drupal::config('ckan_sync.settings');
    $ckan_token = $ckan_settings->get('token');

    $request = \Drupal::request();
    $token = $request->headers->get('X-CKAN-API-Key');

    if ($token == $ckan_token) {
      if ($this->menuExist($name)) {
        $response = $this->getMenu($name);
        $code = 200;
      }
      else {
        $response = t('No response where found');
        $code = 400;
      }
    }
    else {
      $response = t('Specified X-CKAN-API-Key value is incorrect');
      $code = 403;
    }

    return new ResourceResponse($response, $code);
  }

  /**
   * {@inheritdoc}
   */
  private function menuExist($menu_name) {
    $menu_tree = \Drupal::menuTree();
    $parameters = new MenuTreeParameters();
    $manipulators = [['callable' => 'menu.default_tree_manipulators:generateIndexAndSort']];

    if ($menu_tree->load($menu_name, $parameters)) {
      return TRUE;
    }

    return FALSE;
  }

  /**
   * {@inheritdoc}
   */
  private function getMenu($menu_name) {
    $menu_tree = \Drupal::menuTree();
    $parameters = new MenuTreeParameters();
    $manipulators = [['callable' => 'menu.default_tree_manipulators:generateIndexAndSort']];

    $tree = $menu_tree->load($menu_name, $parameters);
    $tree = $menu_tree->transform($tree, $manipulators);
    $menu = $menu_tree->build($tree);

    $menu_data = [
      'type'    => 'menu',
      'name'    => $menu_name,
      'items'   => isset($menu['#items']) ? $this->prepareMenuItems($menu['#items']) : [],
    ];

    return $menu_data;
  }

  /**
   * {@inheritdoc}
   */
  private function prepareMenuItems($items) {
    $items = array_values($items);

    $menu = [];
    if (isset($items)) {
      foreach ($items as $key => $menu_item) {
        $url = $menu_item['url']->toString(TRUE);
        $url_string = $url->getGeneratedUrl();

        $menu_item_data = [
          'label'           => $menu_item['title'],
          'href'            => $url_string,
          'weight'          => $key,
          'is_expanded'     => $menu_item['is_expanded'],
          'is_collapsed'    => $menu_item['is_collapsed'],
          'in_active_trail' => $menu_item['in_active_trail'],
          'children'        => [],
        ];

        $menu_item_data['attributes'] = $this->prepareMenuItemAttributes($menu_item);

        if (!empty($menu_item['below'])) {
          $menu_item_data['children'] = $this->prepareMenuItems($menu_item['below']);
        }

        $menu[] = $menu_item_data;
      }
    }
    return $menu;
  }

  /**
   * {@inheritdoc}
   */
  private function prepareMenuItemAttributes($menu_item) {

    // @todo improve attributes selection.
    $attributes = [
      'id' => isset(
        $menu_item['url']->getOption('attributes')['id']) && $menu_item['url']->getOption('attributes')['id'] ? $menu_item['url']->getOption('attributes')['id'] : '',
      'class' => isset(
        $menu_item['url']->getOption('attributes')['class']) && $menu_item['url']->getOption('attributes')['class'] ? $menu_item['url']->getOption('attributes')['class'][0] : '',
      'target' => isset(
        $menu_item['url']->getOption('attributes')['target']) && $menu_item['url']->getOption('attributes')['target'] ? $menu_item['url']->getOption('attributes')['target'] : '',
      'description' => isset(
        $menu_item['url']->getOption('attributes')['title']) && $menu_item['url']->getOption('attributes')['title'] ? $menu_item['url']->getOption('attributes')['title'] : '',
    ];

    return $attributes;
  }

}
