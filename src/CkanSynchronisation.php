<?php

declare(strict_types=1);

namespace Drupal\ckan_sync;

/**
 * Description of CkanSynchronisation.
 */
class CkanSynchronisation {

  /**
   * Action for api request.
   *
   * @var string
   */
  protected $action;

  /**
   * Parameters for api request.
   *
   * @var array
   */
  protected $params;


  /**
   * Client for api request.
   *
   * @var Drupal\ckan_sync\CkanClient
   */
  protected $skanClient;

  /**
   * {@inheritdoc}
   */
  public function __construct() {
    $this->skanClient = \Drupal::service('ckan_sync.ckan_client');
  }

  /**
   * {@inheritdoc}
   */
  public function getPackageList(int $limit = 0, int $offset = 0): array|null {
    $result = [];

    $this->action = 'package_list';
    $this->params = [
      'limit' => $limit,
      'offset' => $offset,
    ];

    if ($request = $this->skanClient->get($this->action, $this->params)) {
      $result = $request;
    }

    return $result;
  }

  /**
   * {@inheritdoc}
   */
  public function getOrganizationList(int $limit = 0, int $offset = 0, string $sort = 'title asc', $all_fields = 'false'): array|null {
    $result = [];

    $this->action = 'organization_list';
    $this->params = [
      'limit' => $limit,
      'offset' => $offset,
      'sort' => $sort,
      'all_fields' => $all_fields,
    ];

    if ($request = $this->skanClient->get($this->action, $this->params)) {
      $result = $request;
    }

    return $result;
  }

  /**
   * {@inheritdoc}
   */
  public function getGroupList(int $limit = 1000, int $offset = 0, string $sort = 'title asc', $all_fields = 'false'): array|null {
    $result = [];

    $this->action = 'group_list';
    $this->params = [
      'limit' => $limit,
      'offset' => $offset,
      'sort' => $sort,
      'all_fields' => $all_fields,
    ];

    if ($request = $this->skanClient->get($this->action, $this->params)) {
      $result = $request;
    }

    return $result;
  }

  /**
   * {@inheritdoc}
   */
  public function getTagList(string $query = '', string $vocabulary_id = '', $all_fields = 'false'): array|null {
    $result = [];

    $this->action = 'tag_list';
    $this->params = [
      'query' => $query,
      'vocabulary_id' => $vocabulary_id,
      'all_fields' => $all_fields,
    ];

    if ($request = $this->skanClient->get($this->action, $this->params)) {
      $result = $request;
    }

    return $result;
  }

  /**
   * {@inheritdoc}
   */
  public function getDatasetsList(int $rows = 0): array|null {
    $list = [];

    $this->action = 'package_search';
    $this->params = [
      'rows' => $rows,
      'q' => 'type:dataset',
    ];

    if ($request = $this->skanClient->get($this->action, $this->params)) {
      $list = $request['results'];
    }

    return $list;
  }

  /**
   * {@inheritdoc}
   */
  public function getPackage(string $id): array|null {
    $package = [];

    $this->action = 'package_show';
    $this->params = [
      'id' => $id,
    ];

    if ($request = $this->skanClient->get($this->action, $this->params)) {
      $package = $request;
    }

    return $package;
  }

  /**
   * {@inheritdoc}
   */
  public function getGroup(string $id): array|null {
    $group = [];

    $this->action = 'group_show';
    $this->params = [
      'id' => $id,
    ];

    if ($request = $this->skanClient->get($this->action, $this->params)) {
      $group = $request;
    }

    return $group;
  }

  /**
   * {@inheritdoc}
   */
  public function getTag(string $id): array|null {
    $tag = [];

    $this->action = 'tag_show';
    $this->params = [
      'id' => $id,
    ];

    if ($request = $this->skanClient->get($this->action, $this->params)) {
      $tag = $request;
    }

    return $tag;
  }

  /**
   * {@inheritdoc}
   */
  public function getDatasetsCount(): int|null {
    $count = 0;

    $this->action = 'package_search';
    $this->params = [
      'rows' => 0,
      'q' => 'type:dataset',
    ];

    if ($request = $this->skanClient->get($this->action, $this->params)) {
      $count = $request['count'];
    }

    return $count;
  }

  /**
   * {@inheritdoc}
   */
  public function getResourcesCount(): int|null {
    $count = 0;

    $this->action = 'resource_search';
    $this->params = [
      'limit' => 0,
      'query' => 'hash:',
    ];

    if ($request = $this->skanClient->get($this->action, $this->params)) {
      $count = $request['count'];
    }

    return $count;
  }

  /**
   * {@inheritdoc}
   */
  public function getCategoriesCount(): int|null {
    $count = 0;

    $this->action = 'package_search';
    $this->params = [
      'rows' => 0,
      'facet.field' => '["tags"]',
    ];

    if ($request = $this->skanClient->get($this->action, $this->params)) {
      $count = $request['count'];
    }

    return $count;
  }

  /**
   * {@inheritdoc}
   */
  // @TODO Need to request tag link in reslut.
  public function getTopTags(int $limit = 5): array|null {
    $tags = NULL;

    $this->action = 'package_search';
    $this->params = [
      'rows' => 0,
      'facet.field' => '["tags"]',
      'facet.limit' => $limit,
    ];

    if ($request = $this->skanClient->get($this->action, $this->params)) {
      $tags = $request['search_facets']['tags']['items'];
    }

    return $tags;
  }


  /**
   * {@inheritdoc}
   */
  public function getDatasetById(string $id): array|null {
    $datasets = NULL;

    $this->action = 'package_search';
    $this->params = [
      'q' => 'id:' . $id,
    ];

    if ($request = $this->skanClient->get($this->action, $this->params)) {
      $datasets = $request['results'][0];
    }

    return $datasets;
  }

  /**
   * {@inheritdoc}
   */
  public function getCategoriesFilteredCount(array $tags, array $groups, array $organization): int|null {
    $count = 0;
    $queries = [];

    if (!empty($tags)) {
      $queries[] = $this->prepareTerms('tags', $tags);
    }

    if (!empty($groups)) {
      $queries[] = $this->prepareTerms('groups', $groups);
    }

    if (!empty($organization)) {
      $queries[] = $this->prepareTerms('organization', $organization, 'OR');
    }

    if (!empty($queries)) {
      $fq = implode(' AND ', array_filter($queries));

      $this->action = 'package_search';
      $this->params = [
        'rows' => 0,
        'fq' => $fq,
      ];

      if ($request = $this->skanClient->get($this->action, $this->params)) {
        $count = $request['count'] ?? 0;
        \Drupal::logger('ckan_sync')->info('[DONE] Action: @action with params: @params', ['@action' => $this->action, '@params' => json_encode($this->params)]);
      }
      else {
        \Drupal::logger('ckan_sync')->warning('[FAILED] Action: @action with params: @params', ['@action' => $this->action, '@params' => json_encode($this->params)]);
      }
    }

    return $count;
  }

  /**
   * Prepare query part by terms.
   *
   * @param string $taxonomy
   *  Taxonomy name
   * @param array $terms
   *  List of terms in taxonomy
   * @param string $condition
   *  Condition for query
   *
   * @return string|null
   */
  public function prepareTerms(string $taxonomy, array $terms, string $condition = 'AND'): string|null {
    if (!empty($terms)) {
      $filter = function ($tag) {
        return '"' . $tag . '"';
      };

      $tags_prepared = array_map($filter, $terms);
      $query = $taxonomy . ':(' . implode(" $condition ", $tags_prepared) . ')';
    }

    return $query ?? NULL;
  }

}