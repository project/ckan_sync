<?php

declare(strict_types=1);

namespace Drupal\ckan_sync\Form;

use Drupal\Core\Form\ConfigFormBase;
use Drupal\Core\Form\FormStateInterface;

/**
 * Configure CKAN synchronisation settings for this site.
 */
final class CkanSettingsForm extends ConfigFormBase {

  /**
   * {@inheritdoc}
   */
  public function getFormId(): string {
    return 'ckan_sync_ckan_settings';
  }

  /**
   * {@inheritdoc}
   */
  protected function getEditableConfigNames(): array {
    return ['ckan_sync.settings'];
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state): array {
    $ckanService = \Drupal::service('ckan_sync.ckan_client');
    $status = $ckanService->get('status_show');

    $connection = t('Disconnected');
    $color = 'red';
    if (isset($status['ckan_version'])) {
      $connection = t('Connected');
      $color = 'green';
    }

    $form['#prefix'] = '<p class="' . $connection . '">Status: <b class="' . $color . '">' . $connection . '</b></p>';

    $form['url'] = [
      '#type' => 'url',
      '#title' => $this->t('Base URL of CKAN application'),
      '#required' => TRUE,
      '#default_value' => $this->config('ckan_sync.settings')->get('url'),
    ];

    $form['token'] = [
      '#type' => 'textarea',
      '#title' => $this->t('Service API token'),
      '#required' => TRUE,
      '#default_value' => $this->config('ckan_sync.settings')->get('token'),
    ];

    $form['auth_header_name'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Alternative name for authorization header'),
      '#default_value' => $this->config('ckan_sync.settings')->get('auth_header_name'),
      '#description' => t('Use it when application is protected by HTTPAuth. 
          Out of the box CKAN supports following alternative names: X-CKAN-API-Key'),
    ];

    $form['http_auth'] = [
      '#type' => 'details',
      '#title' => t('HTTP auth'),
      '#open' => TRUE,
    ];

    $form['http_auth']['http_auth_username'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Username'),
      '#default_value' => $this->config('ckan_sync.settings')->get('http_auth_username') ?? FALSE,
    ];

    $form['http_auth']['http_auth_pass'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Password'),
      '#default_value' => $this->config('ckan_sync.settings')->get('http_auth_pass') ?? FALSE,
    ];

    return parent::buildForm($form, $form_state);
  }

  /**
   * {@inheritdoc}
   */
  public function validateForm(array &$form, FormStateInterface $form_state): void {
    parent::validateForm($form, $form_state);

    if (filter_var($form_state->getValue('url'), FILTER_VALIDATE_URL) === FALSE) {
      $form_state->setErrorByName('url', t('Not valid url'));
    }
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state): void {
    $this->config('ckan_sync.settings')
      ->set('url', rtrim($form_state->getValue('url'), '/'))
      ->set('token', $form_state->getValue('token'))
      ->set('auth_header_name', $form_state->getValue('auth_header_name'))
      ->set('http_auth_username', $form_state->getValue('http_auth_username'))
      ->set('http_auth_pass', $form_state->getValue('http_auth_pass'))
      ->save();
    parent::submitForm($form, $form_state);
  }

}
