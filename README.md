# Table of contents

- Introduction
- Requirements
- Installation
- Configuration
- Usage
- Maintainers

# CKAN synchronization

The CKAN synchronization module allows You to connect Your drupal site with 
any of CKAN open data management system via rest API. Also this module allows drupal site 
to share the menu structure to CKAN or any other portal via rest endpoint.

# Requirements

This module requires the following modules:

- [Rest](form drupal core)
- [Rest ui](https://www.drupal.org/project/restui)

# Installation

Install as you would normally install a contributed Drupal module. For further
information, see
[Installing Drupal Modules](https://www.drupal.org/docs/extending-drupal/installing-drupal-modules).

# Configuration

Configure the CKAN synchronization connection at (/admin/config/system/ckan-settings).
On the configuration page You have to fill the naxt fields:

- Base URL of CKAN application (Mandatory)
- Service API token (Mandatory, can be obtained from CKAN settings page)
- Alternative name for authorization header (Optional)
- HTTP auth Username (Optional)
- HTTP auth Password (Optional)

If all required fields are filled correctly You should see the Status indicator marked as "Connected".

If You need to provide access to endpoint with menu structure You need to:
- enable the Rest ui module
- enable Menu structure Resource
- set checked token_auth as Authentication provider
- set Permissions for Anonymous user to be accessible to all users,
  in this case Authentication will be provided by token_auth checked on previous step.

# Usage

As a result module provide for developer two different services:

ckan klient - \Drupal::service('ckan_sync.ckan_client');
This service allow developer to make request to CKAN API directly by providing the {action} and {params}
according to the CKAN API documentation, that can be found on https://docs.ckan.org/en/2.9/api/index.html

ckan data import - \Drupal::service('ckan_sync.ckan_data_import');
This service is a wrapper for ckan client service that allows users to use
already prepared requests with human readable names of methods.

Those services can be used inside a custom module or theme for implementation of custom logic.

# Maintainers

Current maintainers:

- [Sergii Shchedrin (sershch)](https://www.drupal.org/u/sershch)

Supporting organizations:

- [Link Digital](https://www.drupal.org/link-digital)
