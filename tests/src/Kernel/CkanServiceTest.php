<?php

namespace Drupal\Tests\ckan_sync\Kernel;

use Drupal\KernelTests\KernelTestBase;

/**
 * Tests the CkanService.
 *
 * @group ckan_sync
 */
class CkanServiceTest extends KernelTestBase {

  /**
   * The service under test.
   *
   * @var \Drupal\ckan_sync\CkanClient
   */
  protected $ckanSynchClient;

  /**
   * The service under test.
   *
   * @var \Drupal\ckan_sync\CkanSynchronisation
   */
  protected $ckanSynchService;

  /**
   * The modules to load to run the test.
   *
   * @var array
   */
  public static $modules = [
    'ckan_sync',
  ];

  /**
   * Create new unit object.
   */
  protected function setUp(): void {
    parent::setUp();

    $this->installConfig(['ckan_sync']);

    $this->ckanSynchClient = \Drupal::service('ckan_sync.ckan_client');

    $this->ckanSynchService = \Drupal::service('ckan_sync.ckan_data_import');
  }

  /**
   * @covers Drupal\ckan_sync\CkanSynchronisation::getOrganizationList
   */
  public function testGetOrganizationList(): void {

    $params = [];
    $list = $this->ckanSynchService->getOrganizationList($params);

    $this->assertIsArray($list);
  }

  /**
   * @covers Drupal\ckan_sync\CkanSynchronisation::getGroupList
   */
  public function testGetGroupList(): void {

    $params = [];
    $list = $this->ckanSynchService->getGroupList($params);

    $this->assertIsArray($list);
  }

  /**
   * @covers Drupal\ckan_sync\CkanSynchronisation::getPackageList
   */
  public function testGetPackageList(): void {

    $params = [];
    $list = $this->ckanSynchService->getPackageList($params);

    $this->assertIsArray($list);
  }

  /**
   * @covers Drupal\ckan_sync\CkanSynchronisation::getTagList
   */
  public function testGetTagList(): void {

    $params = [];
    $list = $this->ckanSynchService->getTagList($params);

    $this->assertIsArray($list);
  }

  /**
   * @covers Drupal\ckan_sync\CkanSynchronisation::getDatasetsCount
   */
  public function testGetDatasetsCount(): void {

    $count = $this->ckanSynchService->getDatasetsCount();

    $this->assertIsInt($count);
  }

  /**
   * Once test method has finished running, tearDown() will be invoked.
   */
  public function tearDown(): void {
    unset($this->ckanSynchService);
  }

}
