<?php

namespace Drupal\Tests\ckan_sync\Unit\Form;

use Drupal\ckan_sync\Form\CkanSettingsForm;
use Drupal\Core\Form\FormInterface;
use Drupal\KernelTests\KernelTestBase;

/**
 * Simple test to ensure that asserts pass.
 *
 * @group ckan_sync
 */
class CkanSettingsFormUnitTest extends KernelTestBase {

  /**
   * Form object.
   *
   * @var \Drupal\ckan_sync\Form
   */
  private $ckanSettingsForm;

  /**
   * Create new unit object.
   */
  public function setUp(): void {
    parent::setUp();

    $this->ckanSettingsForm = new CkanSettingsForm(
      $this->container->get('config.factory'),
      $this->container->get('config.typed')
    );

  }

  /**
   * Tests for \Drupal\ckan_sync\Form\CkanSettingsForm.
   */
  public function testCkanSettingsForm() {
    $this->assertInstanceOf(FormInterface::class, $this->ckanSettingsForm);

    $id = $this->ckanSettingsForm->getFormId();
    $this->assertEquals('ckan_sync_ckan_settings', $id);

    $method = new \ReflectionMethod(CkanSettingsForm::class, 'getEditableConfigNames');
    $method->setAccessible(TRUE);

    $name = $method->invoke($this->ckanSettingsForm);
    $this->assertEquals(['ckan_sync.settings'], $name);
  }

}
